# wb-node-2-test-part-2 (In development)

## Getting Started

LoopBack.js Framework & PostgreSQL

### Prerequisites

Before running this project, make sure you have the following installed:

* Node
* NPM
* StongLoop Controller (*optional*)


### Installing

1. Clone the project

```
git clone git@gitlab.com:norsuzanna/wb-node-2-test-part-2.git
```

Run the project

```
node .
```

## Author

* **Suzanna**

